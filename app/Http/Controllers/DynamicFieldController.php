<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DynamicField;
use Validator;

class DynamicFieldController extends Controller
{
    function index()
    {
     return view('dynamic_field');
    }

    function result(Request $request)
    { 
        $rowNumbers=$request->no_of_rows;
        $colNumbers=$request->no_of_cols;
        foreach($request->statement as $key=>$row)
        {
            $statement_pospair[$key]['statement']=$request->statement[$key];
            $statement_pospair[$key]['position']=$request->position[$key];
        }
        $position = array_column($statement_pospair, 'position');

        array_multisort($position, SORT_ASC, $statement_pospair); // sorting array by position
        
        return view('result', array('statement_pospair'=>$statement_pospair,
                                'colNumbers'=>$colNumbers, 'rowNumbers' =>$rowNumbers  ));
    }

    
}