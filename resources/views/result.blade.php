<html lang="en">
<head>
    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
  <title></title>
          <style>body {
  font: 400 16px 'Muli', sans-serif !important;
  margin: 0;
  padding: 0;
}

header {
  margin: 0;
  max-width: 100%;
  padding: 5px;
  text-align: center;
  overflow: auto;
}

ul{
  list-style-type: none;
  margin: 0 auto;
  padding: 0
}

li{
  background: slategrey;
  display: inline-block;
  margin: 5px;
  padding: 5px 7px;
}

li > a {
  color: white; 
  font-size: 16px;
}

li > a:hover{
  color: #262626;
  text-decoration: none;
}

.inner {
  padding: 30px;
}
/* headings */

.container-fluid h2 {
  font-family: 'Montserrat', sans-serif;
}

.site-title {
  font-size: 50px;
  font-weight: 300;
  text-transform: uppercase;
}

/* text colors */

.black,
.k {
  font-color: #262626;
}

/* background colors */

.sq {
  /*  alignment  */
  float: left;
  margin: 5px;
  /*  size  */
  width: 200px;
  height: 200px;
  /*  box text  */
  color: #262626;
  text-align: center;
}

.sq:hover {
  border: 6px solid rgba(255,255,255, 0.5);
}

.sq p {
  vertical-align: middle;
  text-align: center;
  position: relative;
  top: 40px;
}

.c {
  display: block;
  width: 100px;
  height: 100px;
  border-radius: 100%;
  margin: 10px;
}

/* table */
table{
  margin: 10px auto;
}

</style>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400" rel="stylesheet" type="text/css">

<!--   CSS for 147 Colors   -->
<link href="http://www.colorname.xyz/style.css" rel="stylesheet" type="text/css"> 
    
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<header class="lv-bg">
  <h1 class="site-title"> Dropee </h1>
  <p> assesment result</p>  
</header>
  
          
<!--    color palettes       -->
  
  <div class="container-fluid inner table-responsive">
  <h3><?php echo $rowNumbers." X ".$colNumbers." Table generated with the positions defined and the statements filled"?></h3>
  <table class="table-bordered table-striped">
  <tr class="tableizer-firstrow">
 <?php
$statementcounter = 0;
$posStart = 1;

echo "<table border='5' class='table'>";
for ($i = 0;$i < $rowNumbers;$i++)
{
    echo "<tr>";
    for ($j = 0;$j < $colNumbers;$j++)
    {
        echo "<td> <div class='c am-g'>";
        if ($statementcounter < count($statement_pospair))

        if ($statement_pospair[$statementcounter]['position'] == $posStart) echo $statement_pospair[$statementcounter++]['statement'];
        else echo $posStart;

        else echo $posStart;
        echo "</div></td>";
        $posStart++;
    }
    echo "</tr>";
}
echo "</table>";
?>
 </table>

  </div>
