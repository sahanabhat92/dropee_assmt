<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Laravel 5.8 - DataTables Server Side Processing using Ajax</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   </head>
   <body>
      <div class="container">
         <br />
         <h3 align="center">Dropee assesment form</h3>
         <br />
         <form  id="dynamic_form" action="/result" method="POST">
         <div class="table-responsive">            
               <span id="result"></span>
               <table class="table table-bordered table-striped" id="user_table">
                  
                  <thead>
                  <tr>
                  <td colspan="3" align="center">Generate a table having Rows:  <input type="number"  style="width: 25%;" min="1"  required name="no_of_rows" id="no_of_rows" class="form-control" placeholder="Number of rows" /> 
                                and  Columns:<input type="number" style="width: 25%;"  min="1"  required name="no_of_cols" id="no_of_cols" class="form-control" placeholder="Number of columns" />  </td>
                  </tr>
                     <tr>
                        <th width="35%">statement</th>
                        <th width="35%">position</th>
                        <th width="30%">Action</th>
                     </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                     <tr>
                        <td colspan="2" align="right">&nbsp;</td>
                        <td>
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <input type="submit" name="save" id="save" class="btn btn-primary" value="Save" />
                        </td>
                     </tr>
                  </tfoot>
               </table>            
         </div>
         
         </form>
      </div>
   </body>
</html>

<script>
 
$(document).ready(function(){

 var count = 1;

 dynamic_field(count);

 function dynamic_field(number)
 {
  html = '<tr>';
        html += '<td><input type="text" required name="statement[]" class="form-control" /></td>';
        html += '<td><input type="number"  min="1"  required name="position[]" id="position" class="form-control position" /></td>';
        if(number > 1)
        {
            html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
            $('tbody').append(html);
        }
        else
        {  
            html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
            $('tbody').html(html);
        }
 }

 $(document).on('click', '#add', function(){
  count++;
  dynamic_field(count);
 });

 $(document).on('click', '.remove', function(){
  count--;
  $(this).closest("tr").remove();
 });

$(document).on('blur', '.position', function(){
    var maxpositionno=$("#no_of_rows").val()*$("#no_of_cols").val()
    if($(this).val()>maxpositionno) 
    {
        alert ("Max position number is: "+maxpositionno+" ie. no of rows X no of cols. Please mention position less than max position or increase the no of rows and cols above ");
        $(this).val("") ;
        return false;
    }
    else return true;
    
 });

   

$('form').submit(function(e) {
        var values = $('input[name="position[]"]').map(function() {
        return this.value;
        }).toArray();

        var hasDups = !values.every(function(v,i) {
        return values.indexOf(v) == i;
        });

        if (hasDups) 
        {
            alert("There are statments with duplicate positions, cannot add two or more statements in one position");
            e.preventDefault();
        }
});

 
});

</script>
